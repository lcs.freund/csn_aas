from app import app
from flask_restful import Resource, Api
from .noes import phrases, wildcard_phrases
import random
from flask import request


api = Api(app)

class says_no(Resource):

    def get(self, name=None):
        if not name == None:
            phrase = random.choice(wildcard_phrases)
            return {"data": phrase.format(name)}
        else:
            phrase = random.choice(phrases)
            return {"data": phrase}
    
    def put(self, name=None):
        phrase = request.form['data']
        if "{}" in phrase:
            wildcard_phrases.append(phrase)
            return {"data": "Wildcard-Phrase successfully added"}
        else:
            phrases.append(phrase)
            return {"data": "Phrase successfully added"}
        
        return {"data": "Invalid request!"}

no_routes=[
    '/no',
    '/no-<string:name>'
]
api.add_resource(says_no, *no_routes)

