FROM alpine:latest

RUN apk add --update \
    python3 \
    python3-dev \
    build-base \
    && pip3 install virtualenv \
    && rm -rf /var/cache/apk/*

WORKDIR /app

COPY . /app
RUN virtualenv venv && venv/bin/pip install -r /app/requirements.txt

EXPOSE 5000
CMD [ "venv/bin/python", "run.py" ]