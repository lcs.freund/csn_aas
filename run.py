#!venv/bin/python
import os
from app import app

host = '0.0.0.0'

if __name__ == '__main__':
    if not os.getenv("FLASK_ENV"):
        app.debug = True

    app.run(host=host) # must specify host for the container to be accessible
